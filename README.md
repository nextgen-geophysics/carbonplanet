# Global map of large CO2 emissions facilities

This repository contains an interactive web map that displays large faciliteis with associated CO2 emissions.

<img src="assets/screenshot.png" width="700">

**[VIEW MAP](https://carbon-planet.nextgengeo.com/)**

## Loading data into GIS

It is possible to load the data directly into most GIS program using the GeoJSON file [https://carbon-planet.nextgengeo.com/large-facilities.geojson](https://carbon-planet.nextgengeo.com/large-facilities.geojson).

Add a vector layer in QGIS using the above URL from the layer menu,

<img src="assets/qgis_geojson.png" width="500">

## Building the map


The map uses static HTML/JS, thanks to [deck.gl](https://deck.gl), for serving the map and Python to download and aggregate the data into a single source.

After getting the source build a local Python environment,

```
pip install -r requirements.txt
```

Then generate the JSON file from the data sources. You can also get the latest version [here](https://carbon-planet.nextgengeo.com/large-facilities.json).

```
python get_data.py
```

If you already have a webserver simply copy the entire folder to a folder that your server has access to.

You can also host a local version by modifying all `src` arguments in the `index.html` file to point to a local webserver. E.g. change,

```
<script src="assets/js/mapbox-gl-js-v1.4.0.js"></script>

to

<script src="http://localhost:8000/assets/js/mapbox-gl-js-v1.4.0.js"></script>
```

Repeat that on all `src` links. Then start the local webserver,

```
python server.py
```

And find the map at [http://localhost:8000](http://localhost:8000/index.html).

## Data sources
The map combines information from several regional and national databases,
- [European Pollutant Release and Transfer Register, EU](https://www.eea.europa.eu/data-and-maps/data/industrial-reporting-under-the-industrial-3)
- [Environment and Climate Change, Canada](https://www.canada.ca/en/environment-climate-change/services/environmental-indicators/greenhouse-gas-emissions/large-facilities.html)
- [Facility Level Information on GreenHouse gases Tool (FLIGHT), US](https://ghgdata.epa.gov/ghgp/main.do)
- [Norwegian Environment Agency](https://www.norskeutslipp.no/en/Frontpage/)

## Limitations and caviats
Combining data from different sources comes with several important considerations,

- All sources should be clean and trustworthy. That isn't always the case. Read the `get_data.py:clean_data` function for more.
- Not all facilities are required to report emissions. E.g. in the EU, not all countries have reported their emission numbers for 2019 (the latest year) and in Canada, only very large emitters are included. 
- Most sources records direct (scope 1) emissions. Facilities that use a lot of energy, but don't emit greenhouse gasses (GHG) directly, look "clean" despite using large amounts of energy (scope 2).
- The reporting requirements differ between the different databases. E.g. direct emissions from biomass power plants are included in the Canadian dataset since such plants are considered as carbon-neutral in Canada.
- Due to limited direct measurements/reporting, some sites are estimated using production and energy consumption data. This differs both internally and between datasets. Visit the individual dataset to learn how the numbers are derived.
- There are several ways to estimate CO2 equivalent values when plants emit multiple gasses. Different databases may use slightly different constants. When CO2 equivalence is computed by the `get_data.py` script the values from the Canadian datasets are used. See script for more.

## Notes on processing sources

### Norwegian Environment Agency

The Norwegian dataset doesn't contain location of facilities. The location is estimated using geocoding from the location names. This geocoding is only performed once and stored in the `/data` folder as a cache. See the `get_data.py` file for more.

## License
This source code in this repository is licensed under **The MIT License (MIT)**. Please, read the LICENSE file for more information.

Please review the data sources above for license information on the individual sources.
