#!/usr/bin/env python
"""
Script to combine several sources about GHG emissions

This file isn't the pretties solution to combine the datasets but it works :)
"""

from io import StringIO, BytesIO, TextIOWrapper
from pathlib import Path
from zipfile import ZipFile
from urllib.request import Request, urlopen
import requests
import os

import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from shapely import wkt
import geocoder

COMMON_COLUMNS = [
    'Reporting entity',
    'Facility name',
    'Latitude',
    'Longitude',
    'Total emissions (kt CO2-e)',
    'Facility type',
    'reportingYear'
]

def calc_co2e(entry):
    """Compute total global warming potential

    The range of numbers for GWP for each gas varies a lot from different source. Here the numbers from the Canadian
    government is used to allow comparison between datasets. Only the most common gasses are converted.

    Ref (table 1),
      - https://www.canada.ca/en/environment-climate-change/services/climate-change/greenhouse-gas-emissions/facility-reporting/reporting/technical-guidance-2020/chapter-2.html
      - https://en.wikipedia.org/wiki/Global_warming_potential#Calculating_the_global_warming_potential
    """

    if entry.pollutantCode == 'CO2':
        factor = 1
    elif entry.pollutantCode == 'CH4':
        # Methane
        factor = 25
    elif entry.pollutantCode == 'N2O':
        # Nitrous oxide
        factor = 298
    elif entry.pollutantCode == 'SF6':
        # Sulphur hexafluoride
        factor = 22800
    else:
        # Unknown global warming potential
        factor = 0

    return entry.totalPollutantQuantityKg * factor


def load_no_data():
    """Load data from norskeutslipp, Norwegian emissions

    https://www.norskeutslipp.no/no/Forsiden/
    """
    df_offshore_no = pd.read_csv('data/norwegian_offshore_facilities.csv')
    df_onshore_no = pd.read_csv('data/norwegian_onshore_facilities.csv')

    # Combine and remove facilities without emissions and without a location
    df_no = pd.concat([df_offshore_no, df_onshore_no])
    df_no = df_no[(df_no['Total emissions (kt CO2-e)'] > 0) & (df_no['Latitude'] != 0)]

    return df_no[COMMON_COLUMNS]

def load_us_epa_data():
    """Load data from EPA, USA"""
    print("Loading US dataset")

    if Path('data/flight_facilities.csv').exists() and Path('data/flight_emissions.csv').exists():
        # Load cached value
        print("Loading from cache")
        df_epa_facilities = pd.read_csv('data/flight_facilities.csv')
        df_epa_flight = pd.read_csv('data/flight_emissions.csv')
    else:
        # Load US data from EPA on facilities to get Sector information
        EPA_FLIGHT_FACILITIES_URL = 'https://ghgdata.epa.gov/ghgp/service/export?q=&tr=current&ds=E&ryr=2019&cyr=2019&lowE=-20000&highE=23000000&st=&fc=&mc=&rs=ALL&sc=0&is=11&et=&tl=&pn=undefined&ol=0&sl=0&bs=&g1=1&g2=1&g3=1&g4=1&g5=1&g6=0&g7=1&g8=1&g9=1&g10=1&g11=1&g12=1&s1=1&s2=1&s3=1&s4=1&s5=1&s6=1&s7=1&s8=1&s9=1&s10=1&s201=1&s202=1&s203=1&s204=1&s301=1&s302=1&s303=1&s304=1&s305=1&s306=1&s307=1&s401=1&s402=1&s403=1&s404=1&s405=1&s601=1&s602=1&s701=1&s702=1&s703=1&s704=1&s705=1&s706=1&s707=1&s708=1&s709=1&s710=1&s711=1&s801=1&s802=1&s803=1&s804=1&s805=1&s806=1&s807=1&s808=1&s809=1&s810=1&s901=1&s902=1&s903=1&s904=1&s905=1&s906=1&s907=1&s908=1&s909=1&s910=1&s911=1&sf=11001100&listExport=true'
        # Some facilities have multiple entries (perhaps due to multiple sectors). Just keep a single entry per site.
        df_epa_facilities = pd.read_excel(EPA_FLIGHT_FACILITIES_URL, skiprows=5)\
            .drop_duplicates('GHGRP ID')

        if not os.environ.get('VERCEL_ENV', False):
            # Don't cache in deployment pipeline
            df_epa_facilities.to_csv('data/flight_facilities.csv')

        # Load latest emission data from US data from EPA for 2019
        EPA_FLIGHT_URL = 'https://ghgdata.epa.gov/ghgp/service/export?q=&tr=current&ds=E&ryr=2019&cyr=2019&lowE=-20000&highE=23000000&st=&fc=&mc=&rs=ALL&sc=0&is=11&et=&tl=&pn=undefined&ol=0&sl=0&bs=&g1=1&g2=1&g3=1&g4=1&g5=1&g6=0&g7=1&g8=1&g9=1&g10=1&g11=1&g12=1&s1=1&s2=1&s3=1&s4=1&s5=1&s6=1&s7=1&s8=1&s9=1&s10=1&s201=1&s202=1&s203=1&s204=1&s301=1&s302=1&s303=1&s304=1&s305=1&s306=1&s307=1&s401=1&s402=1&s403=1&s404=1&s405=1&s601=1&s602=1&s701=1&s702=1&s703=1&s704=1&s705=1&s706=1&s707=1&s708=1&s709=1&s710=1&s711=1&s801=1&s802=1&s803=1&s804=1&s805=1&s806=1&s807=1&s808=1&s809=1&s810=1&s901=1&s902=1&s903=1&s904=1&s905=1&s906=1&s907=1&s908=1&s909=1&s910=1&s911=1&sf=11001100&listExport=false'
        df_epa_flight = pd.read_excel(EPA_FLIGHT_URL, skiprows=5)
        df_epa_flight.sort_values('GHG QUANTITY (METRIC TONS CO2e)', ascending=False).head()

        if not os.environ.get('VERCEL_ENV', False):
            # Don't cache in deployment pipeline
            df_epa_flight.to_csv('data/flight_emissions.csv')

    # Validate file contant
    assert df_epa_flight['REPORTING YEAR'].nunique() == 1, 'The CSV file contains more then 1 year! Please clean the data'

    # Combine emission and facility data
    df_epa_flight = df_epa_flight.merge(
        df_epa_facilities[['GHGRP ID', 'SECTORS']],
        on='GHGRP ID',
        how='left'
    )

    # Ensure dataframe has the correct columns
    #
    # Notes from the website about GHG QUANTITY
    # The GHGRP generally requires facilities that emit above 25,000 metric tons CO2e of GHGs to report their emissions.
    # Therefore this data set does not reflect total U.S. emissions or total emissions from individual states.
    # Roughly 50% of total U.S. emissions are reported by large emitting facilities subject to the GHGRP.
    # Additional GHGs are reported by suppliers of fossil fuels and industrial gases.
    #
    # The total number of reporters shown may be less than the sum of the number of reporters in the
    # selected source categories because some facilities fall within more than one source category.
    df_epa_flight.rename(columns={
        'GHG QUANTITY (METRIC TONS CO2e)': 'Total emissions (kt CO2-e)',
        'PARENT COMPANIES': 'Reporting entity',
        'FACILITY NAME': 'Facility name',
        'LATITUDE': 'Latitude',
        'LONGITUDE': 'Longitude',
        'SECTORS': 'Facility type',
    }, inplace=True)
    df_epa_flight['Total emissions (kt CO2-e)'] /= 1000 # Tons to Kilotons
              
    # Add missing columns
    df_epa_flight[['Electricity production (MWh)', 'Primary fuel']] = pd.NA

    df_epa_flight['reportingYear'] = 2019

    # Only keep important columns
    return df_epa_flight[COMMON_COLUMNS]


def load_eu_eea_data():
    """Load data from the EU on large emitters

    Notes
    ------
      - File metadata: https://www.eea.europa.eu/data-and-maps/data/industrial-reporting-under-the-industrial-3/database-structure-and-use-information/eea_industrial_reporting_metadata_v4/at_download/file
    """
    print("Loading EU dataset")

    if Path('data/User friendly Excel files.zip').exists():
        print("Loading from cache")
        # Load cached value
        EEA_LARGE_PLANTS_FILE = 'data/User friendly Excel files.zip'
        zipfile = ZipFile(EEA_LARGE_PLANTS_FILE)
    else:
        # Load data from website
        #EEA_LARGE_PLANTS = 'https://www.eea.europa.eu/data-and-maps/data/industrial-reporting-under-the-industrial-3/user-friendly-tables-in-excel-1/industrial-reporting-db-excel-extracts-1/at_download/file'
        EEA_LARGE_PLANTS = 'https://cmshare.eea.europa.eu/s/GeLzYzM6mXSyAfp/download'
        url = urlopen(EEA_LARGE_PLANTS)
        zipfile = ZipFile(BytesIO(url.read()))

    # Open each file within the zip object and read it into a dataframe
    # Ignore files:
    # - E-PRTR Pollutant Transfers.xlsx
    for file_in_zip in zipfile.NameToInfo.keys():
        if 'Pivot_Excel/F1_4_Detailed releases at facility level with E-PRTR Sector and Annex I Activity detail into Air.xlsx' in file_in_zip:
            df_eea_eprtr_release = pd.read_excel(zipfile.read(file_in_zip))

    # Transform the original wide-format to a long-format.
    df_eea_eprtr_release = df_eea_eprtr_release.melt(
        id_vars=['FacilityInspireID', 'facilityName', 'Latitude', 'Longitude', 'pollutant', 'EPRTRAnnexIMainActivityLabel'],
        value_vars=[2016, 2017, 2018, 2019, 2020],
        var_name='reportingYear',
        value_name='totalPollutantQuantityKg'
    ).query("totalPollutantQuantityKg.notna()")

    df_eea_eprtr_release["pollutantCode"] = df_eea_eprtr_release.pollutant.str.extract('\\((\w*)')

    df_latest_reporting_years = df_eea_eprtr_release\
        .groupby(['FacilityInspireID'])\
        .reportingYear\
        .transform(lambda year: year == year.max())

    df_eea_plant_emissions = df_eea_eprtr_release\
        .loc[df_latest_reporting_years]

    # Compute the GHG equivalent using the global warming potential of each individual gas. Then compute
    # the total emissions per site.
    df_eea_plant_emissions['co2e'] = df_eea_plant_emissions.apply(calc_co2e, axis=1)
    df_eea_single_plant_emissions = df_eea_plant_emissions.groupby('FacilityInspireID').agg({
        "co2e": "sum",
        "facilityName": "first",
        'Latitude': 'first',
        'Longitude': 'first',
        'EPRTRAnnexIMainActivityLabel': 'first',
        'reportingYear': 'first'
    })

    # Clean up units and remove facilities without a GHG footprint
    df_eea_single_plant_emissions['co2e'] /= (1000 * 1000) # Kg to kiloTons
    df_eea_single_plant_emissions.query("co2e > 0", inplace=True)

    # Harmonize columns
    df_eea_single_plant_emissions.rename(columns={
        'co2e': 'Total emissions (kt CO2-e)',
        'facilityName': 'Facility name',
        'EPRTRAnnexIMainActivityLabel': 'Facility type',
    }, inplace=True)
    df_eea_single_plant_emissions["Reporting entity"] = "Unknown"
    df_eea_single_plant_emissions[['Electricity production (MWh)', 'Primary fuel']] = pd.NA

    # Only keep important columns
    df_eea_single_plant_emissions = df_eea_single_plant_emissions[COMMON_COLUMNS]

    return df_eea_single_plant_emissions

def load_ca_data():
    """Load data from the Canadian goverment"""
    print("Loading canadian dataset")
    CA_INFO_URL = 'https://www.canada.ca/en/environment-climate-change/services/environmental-indicators/greenhouse-gas-emissions/large-facilities.html'
    CA_DATA_URL = "https://www.canada.ca/content/dam/eccc/documents/csv/cesindicators/ghg-emissions/2021/greenhouse-gas-emissions-large-facilities.csv"

    if Path('data/ca_large_facilities.csv').exists():
        # Load cached value
        print("Loading from cache")
        df_ca_large_facilities = pd.read_csv('data/ca_large_facilities.csv')
    else:
        # Get data from canada.ca
        resp = requests.get(CA_DATA_URL)
        if resp.status_code != 200:
            raise RuntimeError('Unable to get dataset with error ', resp.text)

        # Read data into Pandas
        raw_csv = StringIO(resp.text)
        df_ca_large_facilities = pd.read_csv(raw_csv,skipfooter=4,
                                            usecols=['Facility name', 'Company name', 'Latitude', 'Longitude',
                                                     'Total emissions', 'Industry classification'])

        if not os.environ.get('VERCEL_ENV', False):
            # Don't cache in deployment pipeline
            df_ca_large_facilities.to_csv('data/ca_large_facilities.csv')

    df_ca_large_facilities.rename(columns={
        'Company name': 'Reporting entity',
        'Total emissions': 'Total emissions (kt CO2-e)',
        'Industry classification': 'Facility type',
    }, inplace=True)
    df_ca_large_facilities[['Electricity production (MWh)', 'Primary fuel']] = pd.NA
    df_ca_large_facilities['reportingYear'] = 2019

    return df_ca_large_facilities[COMMON_COLUMNS]

def load_all_data():
    """Load all datasets"""
    # Create a folder to cache datasets
    Path('data/').mkdir(exist_ok=True)

    # Load all datasets
    data = []
    data.append(load_no_data())
    data.append(load_us_epa_data())
    data.append(load_ca_data())
    data.append(load_eu_eea_data())

    # Produce a single dataframe with all data
    df_merge = pd.concat(data)
    df_merge['coordinates'] = df_merge.apply(lambda x: [x.Longitude, x.Latitude], axis=1)
    df_merge['geometry'] = df_merge.apply(lambda x: Point(x.Longitude, x.Latitude), axis=1)
    df_merge.drop(columns=['Longitude', 'Latitude'], inplace=True)

    # Force to a whole number
    df_merge['Total emissions (kt CO2-e)'] = df_merge['Total emissions (kt CO2-e)'].astype(int)

    return df_merge

def export_data(df):
    """Save combined dataset as both JSON and GeoJSON file"""

    # Output as JSON
    filename = 'large-facilities.json'
    df.loc[:, ~df.columns.isin(['geometry'])].to_json(filename, orient='records', indent=True)
    print(f"Data stored in file {filename}")

    # Create a new dataframe with the expected GeoJSON format
    filename = 'large-facilities.geojson'
    gdf = gpd.GeoDataFrame(df.loc[:, ~df.columns.isin(['coordinates', 'geometry'])], geometry=df.geometry)
    gdf.to_file(filename, driver='GeoJSON', index=False)
    print(f"Data stored in file {filename}")


def clean_data(df):
    """Perform the most needed data cleaning

    This function doesn't ensure the data is 100% clean but removed the worst
    data issues.
    """

    # The PGE Górnictwo plant exists under two different ID's. Remove the oldest.
    df = df[df.index != 'PL.EEA/1298.FACILITY']

    # Remove old facility
    df = df[df.index != 'DK.EEA/6901.FACILITY']

    # Remove this data point as it is clearly mistyped
    #df = df[~df['Facility name'].str.contains("CENTRE D'ENFOUISSEMENT TECHNIQUE DE SAINT JEAN...")]

    # Remove old sites - Facilities that haven't been updated since 2015 are not that trustworthy
    # and therefore removed. This is only relevant for the EU datasets.
    df = df[df.reportingYear >= 2016]

    return df

if __name__ == "__main__":
    df = load_all_data()
    df = clean_data(df)
    print("Total emissions ", df['Total emissions (kt CO2-e)'].sum())
    export_data(df)

    # Exit with success
    exit(0)
